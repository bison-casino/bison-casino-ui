import { FreeLayoutManager, ImageWidget, MoneyWidget, RoundedRectangleWidget, TextWidget } from './bison-canvas.js';
export class PlayerInfoWidget extends RoundedRectangleWidget {
    constructor(dimension, username, balance) {
        super(dimension, 'black', dimension.height / 3);
        this.child = new FreeLayoutManager(dimension, { XRelativeTo: 'left', YRelativeTo: 'top' });
        this.usernameWidget = new TextWidget({ width: dimension.width, height: dimension.height / 2 }, dimension.height / 4, 'white', username);
        this.balanceWidget = new MoneyWidget({ width: dimension.width, height: dimension.height / 2 }, dimension.height / 4, 'white', balance);
        this.child.addWidget(this.usernameWidget, { x: 0, y: 0 });
        this.child.addWidget(this.balanceWidget, { x: 0, y: dimension.height / 2 });
    }
    set username(s) {
        this.usernameWidget.text = s;
    }
    set balance(v) {
        this.balanceWidget.value = v;
    }
    draw(ctx) {
        super.draw(ctx);
        this.child.draw(ctx);
    }
}
function cardToFileName(card) {
    return card.number.toString().concat(card.suit, '.png');
}
const cards = new Map();
const loadCards = () => {
    const root = document.body.getAttribute('data-bg-root');
    const suites = ['spades', 'hearts', 'diamonds', 'clubs'];
    for (let i = 0; i < 4; i++) {
        for (let j = 0; j < 13; j++) {
            let img = new Image();
            img.src = root + 'bg-res/standard-cards/' + cardToFileName({ number: (j + 1), suit: suites[i] });
            cards.set((j + 1) + suites[i], img);
        }
    }
};
loadCards();
export class CardWidget extends ImageWidget {
    constructor(scale, card) {
        super({ width: 128 * scale, height: 208 * scale }, cards.get(card.number + card.suit));
    }
    draw(ctx) {
        super.draw(ctx);
    }
}
