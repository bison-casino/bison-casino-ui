export class Canvas {
    constructor(parentID, bg = 'black', resolution = {
        width: 1920,
        height: 1080
    }) {
        const parent = document.getElementById(parentID);
        if (parent === null) {
            return;
        }
        this.parent = parent;
        this.canvas = document.createElement('canvas');
        this.canvas.width = resolution.width;
        this.canvas.height = resolution.height;
        parent.appendChild(this.canvas);
        this.ctx = this.canvas.getContext('2d');
        this.bg = bg;
        this.resolution = resolution;
        this.resize();
        this.running = true;
    }
    resize() {
        const clearPx = function (s) {
            return parseFloat(s.replaceAll('px', ''));
        };
        const parentCssProps = getComputedStyle(this.parent);
        let innerWidth = clearPx(parentCssProps.width);
        innerWidth -= clearPx(parentCssProps.paddingLeft) + clearPx(parentCssProps.paddingRight);
        let innerHeight = clearPx(parentCssProps.height);
        innerHeight -= clearPx(parentCssProps.paddingTop) + clearPx(parentCssProps.paddingBottom);
        const whRatio = this.resolution.width / this.resolution.height;
        const cwhRatio = innerWidth / innerHeight;
        if (cwhRatio / whRatio > 1) {
            this.canvas.style.width = (innerHeight * whRatio) + 'px';
            this.canvas.style.height = innerHeight + 'px';
        }
        else {
            this.canvas.style.width = innerWidth + 'px';
            this.canvas.style.height = (innerWidth / whRatio) + 'px';
        }
    }
    start() {
        this.running = true;
        window.requestAnimationFrame(this.draw.bind(this));
    }
    stop() {
        this.running = false;
    }
    draw() {
        if (this.root !== undefined) {
            this.ctx.save();
            this.ctx.fillStyle = this.bg;
            this.ctx.fillRect(0, 0, this.resolution.width, this.resolution.height);
            this.ctx.restore();
            this.root.draw(this.ctx);
            if (this.running)
                window.requestAnimationFrame(this.draw.bind(this));
        }
    }
}
export class BaseWidget {
    constructor(dimension) {
        this.dimension = dimension;
    }
    draw(ctx) {
    }
}
export class RoundedRectangleWidget extends BaseWidget {
    constructor(dimension, color, radii) {
        super(dimension);
        this.color = color;
        if (typeof radii === 'number') {
            this.radii = { tl: radii, tr: radii, br: radii, bl: radii };
        }
        else {
            this.radii = Object.assign({ tl: 0, tr: 0, br: 0, bl: 0 }, radii);
        }
    }
    draw(ctx) {
        ctx.save();
        ctx.fillStyle = this.color;
        const r = this.radii;
        const w = this.dimension.width, h = this.dimension.height;
        ctx.beginPath();
        ctx.moveTo(r.tl, 0);
        ctx.lineTo(w - r.tr, 0);
        ctx.quadraticCurveTo(w, 0, w, r.tr);
        ctx.lineTo(w, h - r.br);
        ctx.quadraticCurveTo(w, h, w - r.br, h);
        ctx.lineTo(r.bl, h);
        ctx.quadraticCurveTo(0, h, 0, h - r.bl);
        ctx.lineTo(0, r.tl);
        ctx.quadraticCurveTo(0, 0, r.tl, 0);
        ctx.fill();
        ctx.restore();
    }
}
export class BackgroundContainerWidget extends BaseWidget {
    constructor(child, background) {
        super(child.dimension);
        this.background = background;
        this.child = child;
    }
    draw(ctx) {
        ctx.save();
        if (this.background !== undefined) {
            if (typeof this.background === 'string') {
                ctx.fillStyle = this.background;
                ctx.fillRect(0, 0, this.dimension.width, this.dimension.height);
            }
            else
                ctx.drawImage(this.background, 0, 0, this.dimension.width, this.dimension.height);
        }
        this.child.draw(ctx);
        ctx.restore();
    }
}
export class ImageWidget extends BaseWidget {
    constructor(dimension, img) {
        super(dimension);
        this.image = img;
    }
    draw(ctx) {
        ctx.save();
        ctx.drawImage(this.image, 0, 0, this.dimension.width, this.dimension.height);
        ctx.restore();
    }
}
export class TextWidget extends BaseWidget {
    constructor(dimension, fontSize, color, text) {
        super(dimension);
        this.fontSize = fontSize;
        this.color = color;
        this.text = text === undefined ? '' : text;
    }
    draw(ctx) {
        ctx.save();
        ctx.textBaseline = 'middle';
        ctx.textAlign = 'center';
        ctx.font = this.fontSize + 'px sans-serif';
        ctx.fillStyle = this.color;
        ctx.fillText(this.text, this.dimension.width / 2, this.dimension.height / 2);
        ctx.restore();
    }
}
export class MoneyWidget extends TextWidget {
    constructor(dimension, fontSize, color, value) {
        super(dimension, fontSize, color, MoneyWidget.format(value));
    }
    set value(value) {
        this.text = MoneyWidget.format(value);
    }
    static format(value) {
        if (value === undefined)
            return undefined;
        const sux = ['', 'k', 'm', 'b'];
        let i = 0;
        while (value > 10000 && i < sux.length) {
            value /= 1000;
            i++;
        }
        const str = value.toLocaleString(undefined, {
            minimumFractionDigits: 2,
            maximumFractionDigits: 2,
        });
        return '€ ' + str + sux[i];
    }
}
export const AnimationFunctions = {
    linear: t => t,
    linearReturn: t => -Math.abs(2 * (t - 1 / 2)) + 1,
    parableReturn: t => -4 * (t - 1 / 2) * (t - 1 / 2) + 1,
    ease: t => Math.sin(Math.PI * t - Math.PI / 2) / 2 + 1 / 2,
    easeReturn: t => Math.sin(2 * Math.PI * t - Math.PI / 2) / 2 + 1 / 2,
};
export class Animation extends BaseWidget {
    constructor(child, to, durationMillis, xfn, yfn) {
        super(child.dimension);
        this.lastTime = Date.now();
        this.duration = durationMillis;
        this.currentPosition = { x: 0, y: 0 };
        this.endPosition = to;
        this.child = child;
        this.state = 'stopped';
        this.Xfn = xfn;
        if (yfn === undefined)
            this.Yfn = xfn;
        else
            this.Yfn = yfn;
    }
    getProgress(t) {
        return { xProgress: this.Xfn(t), yProgress: this.Yfn(t) };
    }
    start() {
        this.lastTime = Date.now();
        this.state = 'running';
    }
    draw(ctx) {
        let timeProgress = (Date.now() - this.lastTime) / this.duration;
        ctx.save();
        if (this.state === 'running') {
            if (timeProgress > 1) {
                timeProgress = 1;
                this.state = 'stopped';
            }
            const progress = this.getProgress(timeProgress);
            this.currentPosition.x = this.endPosition.x * progress.xProgress;
            this.currentPosition.y = this.endPosition.y * progress.yProgress;
        }
        ctx.translate(this.currentPosition.x, this.currentPosition.y);
        this.child.draw(ctx);
        ctx.restore();
    }
}
export class FlowLayoutManager {
    constructor(dimension, options) {
        this.opt = Object.assign({
            justify: 'center',
            align: 'center',
            direction: 'row',
            separator: 0,
            autoWidth: false,
            autoHeight: false,
        }, options);
        if (dimension === undefined)
            this.dimension = { width: 0, height: 0 };
        else
            this.dimension = dimension;
        this.children = [];
        this.childrenBound = { width: 0, height: 0 };
        this.lastID = 0;
    }
    calcChildrenPositions() {
        let w = 'width', h = 'height', x = 'x', y = 'y';
        if (this.opt.direction === 'col') {
            w = 'height';
            h = 'width';
            x = 'y';
            y = 'x';
        }
        let sx = 0, sy = 0;
        switch (this.opt.justify) {
            case 'center':
                sx = (this.dimension[w] - this.childrenBound[w]) / 2;
                break;
            case 'end':
                sx = this.dimension[w] - this.childrenBound[w];
                break;
        }
        switch (this.opt.align) {
            case 'center':
                sy = (this.dimension[h] - this.childrenBound[h]) / 2;
                break;
            case 'end':
                sy = this.dimension[h] - this.childrenBound[h];
                break;
        }
        for (const child of this.children) {
            const wd = child.w;
            let p = { x: 0, y: 0 };
            p[x] = sx;
            switch (this.opt.align) {
                case 'start':
                    p[y] = sy;
                    break;
                case 'center':
                    p[y] = sy + (this.childrenBound[h] - wd.dimension[h]) / 2;
                    break;
                case 'end':
                    p[y] = sy + (this.childrenBound[h] - wd.dimension[h]);
                    break;
            }
            child.position = p;
            sx += wd.dimension[w] + this.opt.separator;
        }
    }
    calcChildrenBounds() {
        const dim = { width: 0, height: 0 };
        let w = 'width', h = 'height';
        if (this.opt.direction === 'col') {
            w = 'height';
            h = 'width';
        }
        for (const child of this.children) {
            dim[w] += child.w.dimension[w];
            if (dim[h] < child.w.dimension[h])
                dim[h] = child.w.dimension[h];
        }
        dim[w] += (this.children.length - 1) * this.opt.separator;
        this.childrenBound = dim;
        if (this.opt.autoWidth)
            this.dimension.width = this.childrenBound.width;
        if (this.opt.autoHeight)
            this.dimension.height = this.childrenBound.height;
    }
    addWidget(w) {
        this.lastID++;
        this.children.push({ id: this.lastID, w: w });
        this.calcChildrenBounds();
        this.calcChildrenPositions();
        return this.lastID;
    }
    removeAllWidget() {
        this.children = [];
        this.calcChildrenBounds();
        this.calcChildrenPositions();
    }
    removeWidget(id) {
        let removed = null;
        this.children = this.children.filter(value => {
            if (value.id === id) {
                removed = value.w;
                return false;
            }
            return true;
        });
        this.calcChildrenBounds();
        this.calcChildrenPositions();
        return removed;
    }
    draw(ctx) {
        for (const child of this.children) {
            if (child.position === undefined)
                continue;
            ctx.save();
            ctx.translate(child.position.x, child.position.y);
            child.w.draw(ctx);
            ctx.restore();
        }
    }
}
export class FreeLayoutManager {
    constructor(dimension, options) {
        this.opt = Object.assign({
            XRelativeTo: 'center',
            YRelativeTo: 'center',
        }, options);
        this.dimension = dimension;
        this.children = [];
        this.lastID = 0;
    }
    addWidget(w, position, options) {
        this.lastID++;
        if (position === undefined)
            position = { x: 0, y: 0 };
        const opt = Object.assign(Object.assign({}, this.opt), options);
        this.children.push({ id: this.lastID, w: w, position: position, options: opt });
        return this.lastID;
    }
    removeAllWidget() {
        this.children = [];
    }
    removeWidget(id) {
        let removed = null;
        this.children = this.children.filter(value => {
            if (value.id === id) {
                removed = value.w;
                return false;
            }
            return true;
        });
        return removed;
    }
    draw(ctx) {
        for (const child of this.children) {
            if (child.position === undefined)
                continue;
            const x = child.options.XRelativeTo === 'left' ? child.position.x :
                (child.options.XRelativeTo === 'center' ? child.position.x - child.w.dimension.width / 2 :
                    child.position.x - child.w.dimension.width);
            const y = child.options.YRelativeTo === 'top' ? child.position.y :
                (child.options.YRelativeTo === 'center' ? child.position.y - child.w.dimension.height / 2 :
                    child.position.y - child.w.dimension.height);
            ctx.save();
            ctx.translate(x, y);
            child.w.draw(ctx);
            ctx.restore();
        }
    }
}
